from django.conf.urls import include, url
from qatrack.dashboard import views

app_name = 'dashboard'

urlpatterns = [
    url(r'^hello/$',views.hello,name='hello'),
]
