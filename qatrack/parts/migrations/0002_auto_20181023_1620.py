# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-10-23 22:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('parts', '0001_initial'),
        ('service_log', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='partused',
            name='service_event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='service_log.ServiceEvent'),
        ),
        migrations.AddField(
            model_name='partsuppliercollection',
            name='part',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parts.Part'),
        ),
        migrations.AddField(
            model_name='partsuppliercollection',
            name='supplier',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parts.Supplier'),
        ),
        migrations.AddField(
            model_name='partstoragecollection',
            name='part',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parts.Part'),
        ),
        migrations.AddField(
            model_name='partstoragecollection',
            name='storage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parts.Storage'),
        ),
        migrations.AddField(
            model_name='part',
            name='part_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='parts.PartCategory'),
        ),
        migrations.AddField(
            model_name='part',
            name='storage',
            field=models.ManyToManyField(help_text='Storage locations for this part', related_name='parts', through='parts.PartStorageCollection', to='parts.Storage'),
        ),
        migrations.AddField(
            model_name='part',
            name='suppliers',
            field=models.ManyToManyField(blank=True, help_text='Suppliers of this part', related_name='parts', through='parts.PartSupplierCollection', to='parts.Supplier'),
        ),
        migrations.AlterUniqueTogether(
            name='storage',
            unique_together=set([('room', 'location')]),
        ),
        migrations.AlterUniqueTogether(
            name='room',
            unique_together=set([('site', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='partsuppliercollection',
            unique_together=set([('part', 'supplier')]),
        ),
        migrations.AlterUniqueTogether(
            name='partstoragecollection',
            unique_together=set([('part', 'storage')]),
        ),
    ]
