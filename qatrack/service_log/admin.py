
from django.conf import settings
from django.contrib import admin
import django.forms as forms
from django.forms import ModelForm, ValidationError
from django.utils.translation import ugettext as _

from qatrack.attachments.admin import (
    SaveInlineAttachmentUserMixin,
    get_attachment_inline,
)

from qatrack.qa.admin import (
    UnitFilter,
    FrequencyFilter,
    AssignedToFilter,
    ActiveFilter,
)

from .models import (
    GroupLinker,
    ServiceArea,
    ServiceEventStatus,
    ServiceType,
    ThirdParty,
    UnitServiceArea,
    # PMTask,
    Task,
    UnitTask,
    TaskInstance,
    # TaskList,
)

def unit_name(obj):
    return obj.unit.name
unit_name.admin_order_field = "unit__name"
unit_name.short_description = "Unit"


def freq_name(obj):
    return obj.frequency.name if obj.frequency else "Ad Hoc"
freq_name.admin_order_field = "frequency__name"
freq_name.short_description = "Frequency"


def assigned_to_name(obj):
    return obj.assigned_to.name
assigned_to_name.admin_order_field = "assigned_to__name"
assigned_to_name.short_description = "Assigned To"

class ServiceEventStatusFormAdmin(ModelForm):

    class Meta:
        model = ServiceEventStatus
        fields = '__all__'

    def clean_is_default(self):

        is_default = self.cleaned_data['is_default']
        if not is_default and self.initial.get('is_default', False):
            raise ValidationError('There must be one default status. Edit another status to be default first.')
        return is_default


class DeleteOnlyFromOwnFormAdmin(admin.ModelAdmin):

    def has_delete_permission(self, request, obj=None):
        if obj is None:
            return False
        return super(DeleteOnlyFromOwnFormAdmin, self).has_delete_permission(request, obj)


class ServiceEventStatusAdmin(DeleteOnlyFromOwnFormAdmin):
    list_display = ['name', 'is_review_required', 'is_default', 'rts_qa_must_be_reviewed']
    form = ServiceEventStatusFormAdmin

    class Media:
        js = (
            settings.STATIC_URL + "jquery/js/jquery.min.js",
            settings.STATIC_URL + "colorpicker/js/bootstrap-colorpicker.min.js",
            settings.STATIC_URL + "qatrack_core/js/admin_colourpicker.js",

        )
        css = {
            'all': (
                settings.STATIC_URL + "bootstrap/css/bootstrap.min.css",
                settings.STATIC_URL + "colorpicker/css/bootstrap-colorpicker.min.css",
                settings.STATIC_URL + "qatrack_core/css/admin.css",
            ),
        }

    def delete_view(self, request, object_id, extra_context=None):

        if ServiceEventStatus.objects.get(pk=object_id).is_default:
            extra_context = extra_context or {'is_default': True}

        return super().delete_view(request, object_id, extra_context)


class ServiceTypeAdmin(DeleteOnlyFromOwnFormAdmin):
    list_display = ['name', 'is_review_required', 'is_active']


class ServiceAreaAdmin(DeleteOnlyFromOwnFormAdmin):
    list_display = ['name']
    filter_horizontal = ("units",)


class UnitServiceAreaAdmin(DeleteOnlyFromOwnFormAdmin):
    list_display = ['__str__', 'notes']
    list_filter = ['unit', 'service_area']
    search_fields = ['unit__name', 'service_area__name']


class GroupLinkerAdmin(DeleteOnlyFromOwnFormAdmin):
    list_display = ['name', 'group', 'description', 'help_text']
    list_filter = ['group']
    search_fields = ['name', 'group__name']


class TaskAdmin(admin.ModelAdmin):
    list_display = ["name", "category", "duration_task_time", "modified", "modified_by"]
    search_fields = ["name", "description", "category__name"]
    save_as=True

def ut_unit_name(obj):
    return obj.unit_task.unit.name
ut_unit_name.admin_order_field = "unit_task__unit__name"
ut_unit_name.short_description = "Unit"

class TaskInstanceAdmin(admin.ModelAdmin):
    list_display = ["__str__", ut_unit_name, "task", "work_completed","created_by"]
    list_filter = ["unit_task__unit", "task"]

    def task_name(self, obj):
        return obj.unit_task.task_object.name
    task_name.short_description = _("Task Name")
    task_name.admin_order_field = "unit_task__task_object__name"

    def unit_name(self, obj):
        return obj.unit_task.unit
    unit_name.short_description = _("Unit Name")
    unit_name.admin_order_field = "unit_task__unit__number"


class UnitTaskForm(forms.ModelForm):

    def _clean_readonly(self,f):
        data = self.cleaned_data.get(f, None)

        if self.instance.pk and f in self.changed_data:
            orig = getattr(self.instance, f)
            err_msg = (
                "To prevent data loss, you can not change the Unit, Task "
                "of a UnitTask after it has been created. The original value was: %s"
            ) % orig
            self.add_error(f, err_msg)
        return data

    def clean_unit(self):
        return self._clean_readonly("unit")

class UnitTaskAdmin(admin.ModelAdmin):
    filter_horizontal = ("visible_to",)
    list_display = ['name', unit_name, freq_name, assigned_to_name, "active"]
    list_filter = [UnitFilter, FrequencyFilter, AssignedToFilter, ActiveFilter]
    search_fields = ['name','unit__name','frequency__name']
    change_form_template = "admin/treenav/menuitem/change_form.html"
    list_editable = ["active"]
    save_as = True
    form = UnitTaskForm

    class Media:
        js = (
            settings.STATIC_URL + "js/jquery-1.7.1.min.js",
            settings.STATIC_URL + "js/jquery-ui.min.js",
            settings.STATIC_URL + "js/select2.min.js",
        )

    def get_queryset(self, *args, **kwargs):
        qs = super(UnitTaskAdmin, self).get_queryset(*args, **kwargs)
        return qs.select_related(
            "unit",
            "frequency",
            "assigned_to"
        )



if settings.USE_SERVICE_LOG:
    admin.site.register(ServiceArea, ServiceAreaAdmin)
    admin.site.register(ServiceType, ServiceTypeAdmin)
    admin.site.register(ServiceEventStatus, ServiceEventStatusAdmin)
    admin.site.register(UnitServiceArea, UnitServiceAreaAdmin)
    admin.site.register(GroupLinker, GroupLinkerAdmin)
    admin.site.register(Task, TaskAdmin)
    admin.site.register(UnitTask,UnitTaskAdmin)
    admin.site.register(TaskInstance, TaskInstanceAdmin)

    admin.site.register([ThirdParty], admin.ModelAdmin)
