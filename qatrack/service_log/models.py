import json
import re

from datetime import timedelta, datetime

from django.conf import settings
from django.contrib.auth.models import Group, User
from django.core import urlresolvers
from django.core.serializers.json import DjangoJSONEncoder
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.db.utils import IntegrityError
from django.utils import timezone
from django.utils.translation import ugettext as _
from django_comments.models import Comment
from django.contrib.contenttypes.fields import(
    GenericForeignKey,
    GenericRelation,
)
from django.contrib.contenttypes.models import ContentType
from qatrack.qa.models import Category, Frequency, FrequencyManager, UnitTestCollection, TestListInstance
from qatrack.units.models import NameNaturalKeyManager, Unit, Vendor
from qatrack.qa.testpack import TestPackMixin

re_255 = '([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])'
color_re = re.compile('^rgba\(' + re_255 + ',' + re_255 + ',' + re_255 + ',(0(\.[0-9][0-9]?)?|1)\)$')
validate_color = RegexValidator(color_re, _('Enter a valid color.'), 'invalid')

TOLERANCE = "tolerance"
NO_TOL = "no_tol"
ACTION = "action"
OK = "ok"

NEW_SERVICE_EVENT = 'new_se'
MODIFIED_SERVICE_EVENT = 'mod_se'
STATUS_SERVICE_EVENT = 'stat_se'
CHANGED_RTSQA = 'rtsqa'
PERFORMED_RTS = 'perf_rts'
APPROVED_RTS = 'app_rts'
DELETED_SERVICE_EVENT = 'del_se'
NO_DUE_DATE = NO_TOL
NOT_DUE = OK
DUE = TOLERANCE
OVERDUE = ACTION

LOG_TYPES = (
    (NEW_SERVICE_EVENT, 'New Service Event'),
    (MODIFIED_SERVICE_EVENT, 'Modified Servicew Event'),
    (STATUS_SERVICE_EVENT, 'Service Event Status Changed'),
    (CHANGED_RTSQA, 'Changed Return To Service'),
    (PERFORMED_RTS, 'Performed Return To Service'),
    (APPROVED_RTS, 'Approved Return To Service'),
    (DELETED_SERVICE_EVENT, 'Deleted Service Event')
)


class ServiceArea(models.Model):

    name = models.CharField(max_length=32, unique=True, help_text=_('Enter a short name for this service area'))
    units = models.ManyToManyField(Unit, through='UnitServiceArea', related_name='service_areas')

    objects = NameNaturalKeyManager()

    def natural_key(self):
        return (self.name,)

    def __str__(self):
        return self.name


class UnitServiceArea(models.Model):

    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    service_area = models.ForeignKey(ServiceArea, on_delete=models.CASCADE)

    notes = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = _('Unit Service Area Memberships')
        unique_together = ('unit', 'service_area',)
        ordering = ('unit', 'service_area')

    def __str__(self):
        return '%s :: %s' % (self.unit.name, self.service_area.name)


class ServiceType(models.Model):

    name = models.CharField(max_length=32, unique=True, help_text=_('Enter a short name for this service type'))
    is_review_required = models.BooleanField(default=True, help_text=_('Does this service type require review'))
    is_active = models.BooleanField(default=True, help_text=_('Set to false if service type is no longer used'))
    description = models.TextField(
        max_length=512, help_text=_('Give a brief description of this service type'), null=True, blank=True
    )

    objects = NameNaturalKeyManager()

    def natural_key(self):
        return (self.name,)

    def __str__(self):
        return self.name


class ServiceEventStatus(models.Model):

    name = models.CharField(max_length=32, unique=True, help_text=_('Enter a short name for this service status'))
    is_default = models.BooleanField(
        default=False,
        help_text=_(
            'Is this the default status for all service events? If set to true every other service event '
            'status will be set to false'
        )
    )
    is_review_required = models.BooleanField(
        default=True, help_text=_('Do service events with this status require review?')
    )
    rts_qa_must_be_reviewed = models.BooleanField(
        default=True,
        verbose_name=_("Return To Service (RTS) QA Must be Reviewed"),
        help_text=_(
            'Service events with Return To Service (RTS) QA that has not been reviewed '
            'can not have this status selected if set to true.'
        ),
    )
    description = models.TextField(
        max_length=512, help_text=_('Give a brief description of this service event status'), null=True, blank=True
    )
    colour = models.CharField(default=settings.DEFAULT_COLOURS[0], max_length=22, validators=[validate_color])

    objects = NameNaturalKeyManager()

    class Meta:
        verbose_name_plural = _('Service event statuses')

    def save(self, *args, **kwargs):
        if self.is_default:
            try:
                temp = ServiceEventStatus.objects.get(is_default=True)
                if self != temp:
                    temp.is_default = False
                    temp.save()
            except ServiceEventStatus.DoesNotExist:
                pass
        super(ServiceEventStatus, self).save(*args, **kwargs)

    def natural_key(self):
        return (self.name,)

    def __str__(self):
        return self.name

    @staticmethod
    def get_default():
        try:
            default = ServiceEventStatus.objects.get(is_default=True)
        except ServiceEventStatus.DoesNotExist:
            return None
        return default

    @staticmethod
    def get_colour_dict():
        return {ses.id: ses.colour for ses in ServiceEventStatus.objects.all()}


class ServiceEventManager(models.Manager):

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(is_active=True)

    def get_deleted(self):
        return super().get_queryset().filter(is_active=False)


class ServiceEvent(models.Model):

    unit_service_area = models.ForeignKey(UnitServiceArea, on_delete=models.PROTECT)
    service_type = models.ForeignKey(ServiceType, on_delete=models.PROTECT)
    service_event_related = models.ManyToManyField(
        'self', symmetrical=True, blank=True, verbose_name=_('Related service events'),
        help_text=_('Enter the service event IDs of any related service events.')
    )
    service_status = models.ForeignKey(ServiceEventStatus, verbose_name=_('Status'), on_delete=models.PROTECT)

    user_status_changed_by = models.ForeignKey(User, null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    user_created_by = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT)
    user_modified_by = models.ForeignKey(User, null=True, blank=True, related_name='+', on_delete=models.PROTECT)
    test_list_instance_initiated_by = models.ForeignKey(
        TestListInstance,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='serviceevents_initiated'
    )

    datetime_status_changed = models.DateTimeField(null=True, blank=True)
    datetime_created = models.DateTimeField()
    datetime_service = models.DateTimeField(
        verbose_name=_('Date and time'), help_text=_('Date and time service performed')
    )
    datetime_modified = models.DateTimeField(null=True, blank=True)

    safety_precautions = models.TextField(
        null=True, blank=True, help_text=_('Describe any safety precautions taken')
    )
    problem_description = models.TextField(help_text=_('Describe the problem leading to this service event'))
    work_description = models.TextField(
        null=True, blank=True, help_text=_('Describe the work done during this service event')
    )
    duration_service_time = models.DurationField(
        verbose_name=_('Service time'), null=True, blank=True,
        help_text=_('Enter the total time duration of this service event (Hours : minutes)')
    )
    duration_lost_time = models.DurationField(
        verbose_name=_('Lost time'), null=True, blank=True,
        help_text=_('Enter the total clinical time lost for this service event (Hours : minutes)')
    )
    is_review_required = models.BooleanField(
        default=True, blank=True
    )
    is_active = models.BooleanField(default=True, blank=True)

    objects = ServiceEventManager()

    class Meta:
        get_latest_by = "datetime_service"

        permissions = (
            ('review_serviceevent', 'Can review service event'),
            ('view_serviceevent', 'Can view service event'),
        )

        ordering = ["-datetime_service"]

    def __str__(self):
        return str(self.id)

    def create_rts_log_details(self):
        rts_states = []
        for r in self.returntoserviceqa_set.all():

            utc = r.unit_test_collection
            if not r.test_list_instance:
                state = 'tli_incomplete'
                details = utc.name
            elif not r.test_list_instance.all_reviewed:
                state = 'tli_req_review'
                details = r.test_list_instance.test_list.name
            else:
                state = 'tli_reviewed'
                details = r.test_list_instance.test_list.name

            rts_states.append({'state': state, 'details': details})
        return rts_states

    def set_inactive(self):
        self.is_active = False
        self.save()

        parts_used = self.partused_set.all()
        for pu in parts_used:
            pu.add_back_to_storage()

    def set_active(self):
        self.is_active = True
        self.save()

        parts_used = self.partused_set.all()
        for pu in parts_used:
            pu.remove_from_storage()


class ThirdPartyManager(models.Manager):

    def get_queryset(self):
        return super(ThirdPartyManager, self).get_queryset().select_related('vendor')


class ThirdParty(models.Model):

    vendor = models.ForeignKey(Vendor, on_delete=models.PROTECT)

    first_name = models.CharField(max_length=32, help_text=_('Enter this person\'s first name'))
    last_name = models.CharField(max_length=32, help_text=_('Enter this person\'s last name'))

    objects = ThirdPartyManager()

    class Meta:
        verbose_name = _('Third party')
        verbose_name_plural = _('Third parties')
        unique_together = ('first_name', 'last_name', 'vendor')

    def __str__(self):
        return self.last_name + ', ' + self.first_name + ' (' + self.vendor.name + ')'

    def get_full_name(self):
        return str(self)


class Hours(models.Model):

    service_event = models.ForeignKey(ServiceEvent, on_delete=models.CASCADE)
    third_party = models.ForeignKey(ThirdParty, null=True, blank=True, on_delete=models.PROTECT)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)

    time = models.DurationField(help_text=_('The time this person spent on this service event'))

    class Meta:
        verbose_name_plural = _("Hours")
        unique_together = ('service_event', 'third_party', 'user',)

        default_permissions = ()
        permissions = (
            ("can_have_hours", "Can have hours"),
        )

    def user_or_thirdparty(self):
        return self.user or self.third_party


class ReturnToServiceQAManager(models.Manager):

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).filter(service_event__is_active=True)


class ReturnToServiceQA(models.Model):

    unit_test_collection = models.ForeignKey(
        UnitTestCollection, help_text=_('Select a TestList to perform'), on_delete=models.CASCADE
    )
    test_list_instance = models.ForeignKey(
        TestListInstance, null=True, blank=True, on_delete=models.SET_NULL, related_name='rtsqa_for_tli'
    )
    user_assigned_by = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT)
    service_event = models.ForeignKey(ServiceEvent, on_delete=models.CASCADE)

    datetime_assigned = models.DateTimeField()

    objects = ReturnToServiceQAManager()

    class Meta:
        permissions = (
            ('view_returntoserviceqa', 'Can view return to service qa'),
            ('perform_returntoserviceqa', 'Can perform return to service qa')
        )
        ordering = ['-datetime_assigned']


class GroupLinker(models.Model):

    group = models.ForeignKey(Group, help_text=_('Select the group.'), on_delete=models.CASCADE)

    name = models.CharField(
        max_length=64, help_text=_('Enter this group\'s display name (ie: "Physicist reported to")')
    )
    description = models.TextField(
        null=True, blank=True, help_text=_('Describe the relationship between this group and service events.')
    )
    help_text = models.CharField(
        max_length=64, null=True, blank=True,
        help_text=_('Message to display when selecting user in service event form.')
    )

    class Meta:
        unique_together = ('name', 'group')

    def __str__(self):
        return self.name


class GroupLinkerInstance(models.Model):

    group_linker = models.ForeignKey(GroupLinker, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    service_event = models.ForeignKey(ServiceEvent, on_delete=models.CASCADE)

    datetime_linked = models.DateTimeField()

    class Meta:
        default_permissions = ()
        unique_together = ('service_event', 'group_linker')


class JSONField(models.TextField):

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, str):
                return json.loads(value)
        except ValueError:
            pass
        return value

    def from_db_value(self, value, *args):
        return self.to_python(value)

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == "":
            return None
        if isinstance(value, dict):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return value


class ServiceLogManager(models.Manager):

    def log_new_service_event(self, user, instance):
        self.create(
            user=user,
            service_event=instance,
            log_type=NEW_SERVICE_EVENT,
        )

    def log_changed_service_event(self, user, instance, extra_info):
        self.create(
            user=user,
            service_event=instance,
            log_type=MODIFIED_SERVICE_EVENT,
            extra_info=json.dumps(extra_info)
        )

    def log_service_event_status(self, user, instance, extra_info, status_change):

        self.create(
            user=user,
            service_event=instance,
            log_type=STATUS_SERVICE_EVENT,
            extra_info=json.dumps({'status_change': status_change, 'other_changes': extra_info})
        )

    def log_rtsqa_changes(self, user, instance):

        self.create(
            user=user,
            service_event=instance,
            log_type=CHANGED_RTSQA,
            extra_info=json.dumps(instance.create_rts_log_details())
        )

    def log_service_event_delete(self, user, instance, extra_info):

        self.create(
            user=user,
            service_event=instance,
            log_type=DELETED_SERVICE_EVENT,
            extra_info=json.dumps(extra_info)
        )


class ServiceLog(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_event = models.ForeignKey(ServiceEvent, on_delete=models.CASCADE)

    log_type = models.CharField(choices=LOG_TYPES, max_length=10)

    extra_info = JSONField(blank=True, null=True)
    datetime = models.DateTimeField(default=timezone.now, editable=False)

    objects = ServiceLogManager()

    class Meta:
        ordering = ('-datetime',)
        default_permissions = ()

class TaskManager(models.Manager):

    def get_by_natural_key(self, name):
        return self.get(name=name)

class TaskInterface(models.Model):

    name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(
        help_text=_("A concise description of this task. (You may use HTML markup)"),
        null=True,
        blank=True,
    )
    javascript = models.TextField(
        help_text=_('Any extra javascript to run when loading perform page'),
        null=True,
        blank=True,
    )

    assigned_to = GenericRelation(
        "UnitTask",
        content_type_field="content_type",
        object_id_field="object_id",
    )

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_created", editable=False, null=True)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_modified", editable=False, null=True)

    class Meta:
        abstract = True

    def get_task(self, day=0):
        return 0, self

    def next_task(self, day):
        return 0, self

    def first(self):
        return self

    def task_members(self):
        raise NotImplementedError

    def content_type(self):
        return ContentType.objects.get_for_model(self)

class Task(TaskInterface):

    procedure = models.CharField(
            max_length=512,
            help_text=_("Link to document describing how to perform this test"),
            blank=True,
            null=True,
        )
    category = models.ForeignKey(Category, help_text=_("Choose a category for this test"), blank=True, null=True,related_name='task_category')
    duration_task_time = models.DurationField(
            default=timedelta(hours=1),
            help_text=_("Length of time needed to complete Task (hh:mm:ss)"),
            blank=True,
        )
    ut = GenericRelation('UnitTask', related_query_name='task')

    objects = TaskManager()

    def __str__(self):
        """return display representation of object"""
        return "%s" % (self.name)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Task, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )
        self.ut.update(name=self.name)

class UnitTaskManager(models.Manager):
    def by_unit(self, unit):
        return self.get_queryset().filter(unit=unit)

    def by_frequency(self, frequency):
        return self.get_queryset().filter(frequency=frequency)

    def by_unit_frequency(self, unit, frequency):
        return self.by_frequency(frequency).filter(unit=unit)

    def get_task(self):
        return self.get_queryset().filter(
            content_type = ContentType.objects.get(app_label='service_log', model='task')
        )


    def by_visibility(self, groups):
        return self.get_queryset().filter(visible_to__in=groups)


class UnitTask(models.Model):

    unit = models.ForeignKey(Unit)

    frequency = models.ForeignKey(
        Frequency,
        help_text=_("Frequency with which this Preventative Maintenance task is to be performed"),
        null=True,
        blank=True,
        related_name='unittask'
    )
    due_date = models.DateTimeField(help_text=_("Next time this item is due"), null=True, blank=True)
    auto_schedule = models.BooleanField(
        help_text=_("If this is checked, due_date will be auto set based on the assigned frequency"),
        default=True,
    )
    assigned_to = models.ForeignKey(
        Group,
        help_text=_("QA group that this test list should nominally be performed by"),
        null=True,
        related_name='unittasks',
    )
    visible_to = models.ManyToManyField(
        Group,
        help_text=_("Select groups who will be able to see this task on this unit"),
        related_name="task_visibility",
    )

    active = models.BooleanField(help_text=_("Uncheck to disable this test on this unit"), default=True, db_index=True)

    limit = Q(app_label='service_log', model='task')
    content_type = models.ForeignKey(
        ContentType,
        limit_choices_to=limit,
        verbose_name = 'Content Type',
        help_text='',
        default=ContentType.objects.get(app_label='service_log', model='task').id
        )
    object_id = models.PositiveIntegerField(
        verbose_name = "Task ID",
        help_text="The task object assigned to the unit",
        blank=True,
        null=True,
    )
    task_object = GenericForeignKey('content_type', 'object_id')
    objects = UnitTaskManager()
    name = models.CharField(max_length=255, db_index=True, default='', editable=False)

    last_instance = models.ForeignKey("TaskInstance", null=True, editable=False, on_delete=models.SET_NULL)

    class Meta:
        verbose_name_plural = _('Unit Task Memberships')
        unique_together = ('unit','frequency','content_type', 'object_id',)

    def calc_due_date(self):
        """return the next due date of this Unit/Task pair """
        if self.auto_schedule and self.frequency is not None:
            last_valid = self.last_valid_instance()
            if last_valid is None and self.last_instance is not None:
                return timezone.now()
            elif last_valid is not None and last_valid.work_completed:
                return last_valid.work_completed + self.frequency.due_delta()

        return self.due_date

    def set_due_date(self, due_date=None):
        """Set due date field for this UT. Note mdel is not saved to db.
        Saving to be done manually"""
        if self.auto_schedule and due_date is None and self.frequency is not None:
            due_date = self.calc_due_date()

        if due_date is not None:
            self.due_date = due_date
            UnitTask.objects.filter(pk=self.pk).update(due_date=due_date)

    def due_status(self):
        if not self.due_date:
            return NO_DUE_DATE

        today = timezone.localtime(timezone.now()).date()
        due = timezone.localtime(self.due_date).date()

        if today < due:
            return NOT_DUE

        if self.frequency is not None:
            overdue = due + timezone.timedelta(days=self.frequency.overdue_interval - self.frequency.due_interval)
        else:
            overdue = due + timezone.timedelta(days=1)

        if today < overdue:
            return DUE
        return OVERDUE

    def last_valid_instance(self):
        """return last task """
        try:
            return self.taskinstance_set.filter(
                in_progress=False,
            ).latest("work_completed")
        except TaskInstance.DoesNotExist:
            pass

    def last_done_date(self):
        """return date this task was last performed"""

        if hasattr(self, "last_instance") and self.last_instance is not None:
            return self.last_instance.work_completed

    def history(self, before=None):
        before = before or timezone.now()

        tis = TaskInstance.objects.filter(unit_task=self)

        if before is not None:
            tis = tis.filter(work_completed__lt=before)

        tis = tis.order_by(
            "-work_completed"
        ).prefetch_related(
            "taskinstance_set__unit_task__unit",
            "taskinstance_set__task",
            "taskinstance_set__created_by",
        )[:settings.NHIST]

        intances = tis.values_list("object_id", flat=True)
        dates = tis.values_list("work_completed", flat=True)

        return instances, dates


    def next_task(self):
        """return next task to be completed from task_object"""
        if not hasattr(self, "last_instance") or not self.last_instance:
            first = self.task_object.first()
            if not first:
                return None, None
            return 0, first

        return self.task_object.next_task(self.last_instance.day)

    def get_task(self, day=None):
        """return day and next task to be completed from task_object"""
        if day is None:
            return self.next_task()

        return self.task_object.get_task(day)

    def get_absolute_url(self):
        return urlresolvers.reverse("perform_pm",kwargs={"pk": self.pk})

    def __str__(self):
        return '%s :: %s' % (self.unit.name, self.task_object)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.name = self.task_object.name
        super(UnitTask, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields,
        )

class TaskInstanceManager(models.Manager):

    def in_progress(self):
        return self.get_queryset().filter(in_progress=True).order_by("-work_completed")

    def complete(self):
        return self.get_queryset().filter(in_progress=False).order_by("-work_completed")


class TaskInstance(models.Model):
    """Container for a Service Log : model: Tasks

    When a user completes a task, a :model:`TaskInstance` is created
    """
    unit_task = models.ForeignKey(UnitTask, editable=False)
    task = models.ForeignKey(Task, editable=False)

    work_started = models.DateTimeField(db_index=True)
    work_completed = models.DateTimeField(default=timezone.now, db_index=True, null=True)

    value = models.FloatField(
        help_text=_("For boolean Tests a value of 0 equals False and any non zero equals True"),
        null=True,
    )

    comment = models.TextField(help_text=_("Add a comment to this task"), null=True, blank=True)

    due_date = models.DateTimeField(
        null=True,
        blank=True,
        help_text=('When was this session due when it was performed'),
    )

    comments = GenericRelation(Comment, object_id_field='object_pk')

    in_progress = models.BooleanField(
        help_text=_(
            "Mark this session as still in progress so you can complete later (will not be submitted for review)"
        ),
        default=False,
        db_index=True,
    )

    day = models.IntegerField(default=0)

    created=models.DateTimeField(auto_now_add=True)
    created_by=models.ForeignKey(User, editable=False, related_name='task_instance_creator')
    modified=models.DateTimeField()
    modified_by = models.ForeignKey(User, editable=False, related_name='task_instance_modifier')

    objects = TaskInstanceManager()

    class Meta:
        get_latest_by = "work_completed"
        permissions = (
            ("can_view_history", "Can see task history when performing PM Task"),
        )

    def duration(self):
        """return timedelta of time from start to completion"""
        return self.work_completed - self.work_started

    def get_value(self):
        return bool(self.value)

    def value_display(self):
        task = self.unit_task.task_object
        return "Yes" if int(self.value) == 1 else "No"


    def __str__(self):
        return "TaskInstance(pk=%s)" % self.pk

    def str_verbose(self):
        return '%s (%s - %s)' % (
            self.pk, self.task.name, timezone.localtime(self.created).strftime('%b %m, %I:%M %p')
        )

@receiver(pre_save, sender=Hours, dispatch_uid="qatrack.service_log.models.ensure_hours_unique")
def ensure_hours_unique(sender, instance, raw, using, update_fields, **kwargs):
    """Some DB's don't consider multiple rows which contain the same columns
    and include null to violate unique contraints so we do our own check"""

    if instance.id is None:
        try:
            Hours.objects.get(service_event=instance.service_event, third_party=instance.third_party, user=instance.user)
        except Hours.DoesNotExist:
            pass
        else:
            # not a unique Hours object
            raise IntegrityError
