import collections

from django import template
from django.conf import settings
from django.template.loader import get_template
from django.utils.safestring import mark_safe

import qatrack.service_log.models as models

register = template.Library()

@register.simple_tag
def pm_value_form(form, task, perms, unit_task=None, task_info=None):
    template=get_template("service_log/pmvalue_form.html")
    c = {
        "form": form,
        "perms": perms,
        "task": task,
        "task_info": task_info,
        "unit_task": unit_task,
    }
    return template.render(c)

@register.simple_tag
def pm_table_colspan(perms, offset=0):
    perms_to_check = ['can_view_history',]
    span = 6 - offset + sum(1 for p in perms_to_check if perms['pm'][p])
    return "%d" % (span)

@register.simple_tag
def history_display(history, unit, task, frequency=None):
    template = get_template("service_log/history.html")
    c = {
        "history": history,
        "unit": unit,
        "task": task,
        "show_icons": settings.ICON_SETTINGS['SHOW_STATUS_ICONS_HISTORY'],
        "frequency": frequency
    }
    return template.render(c)

@register.filter(expects_local_time=True)
def as_due_date(unit_task):
    template = get_template('service_log/due_date.html')
    c = {"unit_task": unit_task, "show_icons": settings.ICON_SETTINGS['SHOW_DUE_ICONS']}
    return template.render(c)

@register.filter(is_safe=True, expects_local_time=True)
def as_time_delta(time_delta):
    hours, remainder = divmod(time_delta.seconds, 60 * 60)
    minutes, seconds = divmod(remainder, 60)
    return '%dd %dh %dm %ds' % (time_delta.days, hours, minutes, seconds)
as_time_delta.safe=True

@register.filter
def as_data_attributes(unit_task):
    ut = unit_task
    due_date = ut.due_date
    last_done = ut.last_done_date()

    attrs = {
        "frequency": ut.frequency.slug,
        "due_date": due_date.isoformat() if due_date else "",
        "last_done": last_done.isoformat() if last_done else "",
        "id": ut.pk,
        "unit_number": ut.unit.number,
    }

    return " ".join(['data-%s=%s' % (k, v) for k, v in list(attrs.items()) if v])

@register.filter
def hour_min(duration):
    total_seconds = int(duration.total_seconds())
    hours = total_seconds // 3600
    minutes = (total_seconds % 3600) // 60
    return '{:0>2}:{:0>2}'.format(hours, minutes)
